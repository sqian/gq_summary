#!/bin/bash
python make_plot_cms.py --cms_text ' ' --goms 0.05,0.1,0.3,0.5,1.0  --logy --logx --save_tag "_review_scout" --conference_label " "  --analyses "\
EXO16056_narrow_lowmass_obs,\
EXO14005_obs,\
EXO19004_obs,\
EXO17027_obs,\
EXO18012_AK8_obs,\
EXO16057_SR1_obs,\
EXO16056_narrow_highmass_obs,\
EXO19012_obs,\
EXO16046_obs"
