# How to run the script?

```
cmsrel CMSSW_10_6_19
cd CMSSW_10_6_19/src
git clone https://gitlab.cern.ch/sqian/gq_summary.git ExoDMSummaryPlots
cmsenv
scram b
cd GQSummary/review_paper
source ./moriond2021.sh
```
now created plots are in `$CMSSW_BASE/src/GQSummary/plots` directory
